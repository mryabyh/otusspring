package ru.otus.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(value = {SpringExtension.class})
@TestPropertySource({"classpath:test.yaml"})
public class LocaleMessageServiceImplEnTest {

    public LocaleMessageServiceImplEnTest(@Value("${uiFileName}") String uiFileName) {
        this.localeMessageService = new LocaleMessageServiceImpl("en", "US", uiFileName, testMessageSource());
    }

    private LocaleMessageService localeMessageService;

    @Test
    void getGreetingsMessage() {
        assertEquals(localeMessageService.getGreetingsMessage(), "Test hello");
    }

    @Test
    void getEnterFirstNameMessage() {
        assertEquals(localeMessageService.getEnterFirstNameMessage(), "Test firstname");
    }

    @Test
    void getEnterLastNameMessage() {
        assertEquals(localeMessageService.getEnterLastNameMessage(), "Test lastname");
    }

    @Test
    void getEnterRightNumberMessage() {
        assertEquals(localeMessageService.getEnterRightNumberMessage(), "Test answer number");
    }

    @Test
    void getEnterTryAgainMessage() {
        assertEquals(localeMessageService.getEnterTryAgainMessage(), "Test try again");
    }

    @Test
    void getResultMessage() {
        int rightAnswers = 1;
        int wrongAnswers = 2;

        assertEquals(localeMessageService.getResultMessage(rightAnswers, wrongAnswers)
                , "Test answers " + rightAnswers + " " + wrongAnswers);
    }

    @Test
    void getStudentNameMessage() {
        String firstName = "A";
        String lastName = "B";

        assertEquals(localeMessageService.getStudentNameMessage(firstName, lastName)
                , "Test results " + firstName + " " + lastName);
    }

    @Configuration
    static class Config {

        @Bean
        public static PropertySourcesPlaceholderConfigurer propertiesResolver() {
            return new PropertySourcesPlaceholderConfigurer();
        }
    }

    @Bean(name = "messageSource")
    private MessageSource testMessageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }
}
