package ru.otus.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import ru.otus.dao.*;
import ru.otus.domain.Question;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class QuestionServiceImplTest {

    private QuestionService questionService;

    @BeforeEach
    private void setUp() {
        MockitoAnnotations.initMocks(this);
        final LoaderDao loaderDao = mock(LoaderDaoImpl.class);

        final ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();

        final InputStream asIs = systemClassLoader.getResourceAsStream("answers_en_US.csv");
        when(loaderDao.loadAnswers()).thenReturn(new InputStreamReader(asIs, StandardCharsets.UTF_8));

        final AnswerDao answerDao = new AnswerDaoImpl(loaderDao);

        final InputStream questionsIs = systemClassLoader.getResourceAsStream("questions_en_US.csv");
        when(loaderDao.loadQuestions()).thenReturn(new InputStreamReader(questionsIs, StandardCharsets.UTF_8));

        final QuestionDao questionDao = new QuestionDaoImpl(loaderDao);

        questionService = new QuestionServiceImpl(questionDao, answerDao);
    }

    @Test
    void getAllQuestions() {
        final Collection<Question> allQuestions = questionService.getAllQuestions();
        assertNotNull(allQuestions);
        assertEquals(5, allQuestions.size());
    }
}