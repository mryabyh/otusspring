package ru.otus.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(value = {SpringExtension.class})
@TestPropertySource({"classpath:test.yaml"})
class LocaleMessageServiceImplRuTest {

    public LocaleMessageServiceImplRuTest(@Value("${uiFileName}") String uiFileName) {
        this.localeMessageService = new LocaleMessageServiceImpl("ru", "RU", uiFileName, testMessageSource());
    }

    private LocaleMessageService localeMessageService;

    @Test
    void getGreetingsMessage() {
        assertEquals(localeMessageService.getGreetingsMessage(), "Тест приветствия");
    }

    @Test
    void getEnterFirstNameMessage() {
        assertEquals(localeMessageService.getEnterFirstNameMessage(), "Тест имени");
    }

    @Test
    void getEnterLastNameMessage() {
        assertEquals(localeMessageService.getEnterLastNameMessage(), "Тест фамилии");
    }

    @Test
    void getEnterRightNumberMessage() {
        assertEquals(localeMessageService.getEnterRightNumberMessage(), "Тест ввода номера правильного ответа");
    }

    @Test
    void getEnterTryAgainMessage() {
        assertEquals(localeMessageService.getEnterTryAgainMessage(), "Тест повторной попытки");
    }

    @Test
    void getResultMessage() {
        int rightAnswers = 1;
        int wrongAnswers = 2;

        assertEquals(localeMessageService.getResultMessage(rightAnswers, wrongAnswers)
                , "Тест отображения результата " + rightAnswers + " " + wrongAnswers);
    }

    @Test
    void getStudentNameMessage() {
        String firstName = "A";
        String lastName = "B";

        assertEquals(localeMessageService.getStudentNameMessage(firstName, lastName)
                , "Тест имени студента " + firstName + " " + lastName);
    }

    @Bean(name = "messageSource")
    private MessageSource testMessageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }
}
