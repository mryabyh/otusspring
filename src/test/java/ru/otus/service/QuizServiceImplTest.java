package ru.otus.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import ru.otus.dao.LoaderDao;
import ru.otus.dao.LoaderDaoImpl;
import ru.otus.domain.Answer;
import ru.otus.domain.Question;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class QuizServiceImplTest {

    private QuizService quizService;
    private ConsoleUserInterface consoleUserInterface;
    private LocaleMessageService localeMessageService;

    @BeforeEach
    private void setUp() {
        MockitoAnnotations.initMocks(this);
        consoleUserInterface = mock(ConsoleUserInterface.class);
        localeMessageService = mock(LocaleMessageServiceImpl.class);

        quizService = new QuizServiceImpl(consoleUserInterface, localeMessageService);
    }

    @Test
    void oneQuestionWithIncorrectAnswer() {
        String rightAnswer = "rightAnswer";
        Question question = new Question(1, "1", rightAnswer);
        when(consoleUserInterface.getLineFromUser()).thenReturn("qwerty");

        assertFalse(quizService.processOneQuestion(question));
    }

    @Test
    void oneQuestionWithWrongAnswer() {
        String rightAnswer = "rightAnswer";
        Question question = new Question(1, "1", rightAnswer);
        question.addAnswers(Arrays.asList(new Answer("1"), new Answer("2"), new Answer(rightAnswer)));
        when(consoleUserInterface.getLineFromUser()).thenReturn("1");

        assertFalse(quizService.processOneQuestion(question));
    }

    @Test
    void oneQuestionWithRightAnswer() {
        String rightAnswer = "rightAnswer";
        Question question = new Question(1, "1", rightAnswer);
        question.addAnswers(Arrays.asList(new Answer("1"), new Answer("2"), new Answer(rightAnswer)));
        when(consoleUserInterface.getLineFromUser()).thenReturn("3");

        assertTrue(quizService.processOneQuestion(question));
    }
}