package ru.otus.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class ConsoleUserInterfaceTest {

    private ConsoleUserInterface userInterfaceService;

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @AfterEach
    private void restoreStreams() {
        System.setOut(originalOut);
    }

    @BeforeEach
    private void setUp() {
        System.setOut(new PrintStream(outContent));
        userInterfaceService = new ConsoleUserInterface();
    }

    @Test
    void getLineFromUser() {
        String input = "add 5";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        assertEquals("add 5", userInterfaceService.getLineFromUser());
    }

    @Test
    void printLineToUser() {
        String testString = "Hello";
        userInterfaceService.printLineToUser(testString);

        StringWriter expectedStringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(expectedStringWriter);
        printWriter.println(testString);
        printWriter.close();

        String expected = expectedStringWriter.toString();

        assertEquals(expected, outContent.toString());
    }

}