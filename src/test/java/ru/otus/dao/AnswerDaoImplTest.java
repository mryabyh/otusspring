package ru.otus.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import ru.otus.domain.Answer;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AnswerDaoImplTest {

    private AnswerDao answerDao;

    @BeforeEach
    private void setUp() {
        MockitoAnnotations.initMocks(this);
        final LoaderDao loaderDao = mock(LoaderDaoImpl.class);

        final ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();

        final InputStream asIs = systemClassLoader.getResourceAsStream("answers_en_US.csv");
        when(loaderDao.loadAnswers()).thenReturn(new InputStreamReader(asIs, StandardCharsets.UTF_8));

        answerDao = new AnswerDaoImpl(loaderDao);
    }

    @Test
    void loadAllQuestions() {
        final Map<Integer, Collection<Answer>> answers = answerDao.loadAllAnswers();
        assertNotNull(answers);
        assertEquals(5, answers.size());
    }
}
