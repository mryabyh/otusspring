package ru.otus.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import ru.otus.domain.Question;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class QuestionDaoImplTest {

    private QuestionDao questionDao;

    @BeforeEach
    private void setUp() {
        MockitoAnnotations.initMocks(this);
        final LoaderDao loaderDao = mock(LoaderDaoImpl.class);

        final ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();

        final InputStream questionsIs = systemClassLoader.getResourceAsStream("questions_en_US.csv");
        when(loaderDao.loadQuestions()).thenReturn(new InputStreamReader(questionsIs, StandardCharsets.UTF_8));

        questionDao = new QuestionDaoImpl(loaderDao);
    }

    @Test
    void loadAllQuestions() {
        final Collection<Question> questions = questionDao.loadAllQuestions();
        assertNotNull(questions);
        assertEquals(5, questions.size());
    }

}