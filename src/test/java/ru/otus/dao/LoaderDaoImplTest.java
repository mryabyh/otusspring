package ru.otus.dao;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.InputStreamReader;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(value = {SpringExtension.class})
@TestPropertySource({"classpath:test.yaml"})
public class LoaderDaoImplTest {

    public LoaderDaoImplTest(@Value("${path-to-questions}") String pathToQuestions,
                             @Value("${path-to-answers}") String pathToAnswers) {
        loaderDaoRu = new LoaderDaoImpl(pathToQuestions, pathToAnswers, "ru", "RU");
        loaderDaoEn = new LoaderDaoImpl(pathToQuestions, pathToAnswers, "en", "US");
    }

    private LoaderDao loaderDaoRu;
    private LoaderDao loaderDaoEn;

    @Test
    void testLoadQuestions() {
        final InputStreamReader ruQuestions = loaderDaoRu.loadQuestions();
        final InputStreamReader enQuestions = loaderDaoEn.loadQuestions();
        assertNotNull(ruQuestions);
        assertNotNull(enQuestions);
        assertNotEquals(ruQuestions, enQuestions);
    }

    @Test
    void testLoadAnswers() {
        final InputStreamReader ruAnswers = loaderDaoRu.loadAnswers();
        final InputStreamReader enAnswers = loaderDaoEn.loadAnswers();
        assertNotNull(ruAnswers);
        assertNotNull(enAnswers);
        assertNotEquals(ruAnswers, enAnswers);
    }

    @Configuration
    static class Config {

        @Bean
        public static PropertySourcesPlaceholderConfigurer propertiesResolver() {
            return new PropertySourcesPlaceholderConfigurer();
        }
    }
}
