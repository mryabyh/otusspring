package ru.otus.dao;

import java.io.InputStreamReader;

public interface LoaderDao {

    InputStreamReader loadQuestions();

    InputStreamReader loadAnswers();
}
