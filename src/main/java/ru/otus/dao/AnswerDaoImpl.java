package ru.otus.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.otus.domain.Answer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

@Component
public class AnswerDaoImpl implements AnswerDao {
    private final LoaderDao loaderDao;

    public AnswerDaoImpl(LoaderDao loaderDao) {
        this.loaderDao = loaderDao;
    }

    @Override
    public Map<Integer, Collection<Answer>> loadAllAnswers() {
        Map<Integer, Collection<Answer>> answers = new HashMap<>();
        InputStreamReader streamReader = loaderDao.loadAnswers();
        BufferedReader reader = new BufferedReader(streamReader);
        try {
            for (String line; (line = reader.readLine()) != null; ) {
                final List<String> splittedString = Arrays.asList(line.split(";"));
                final Integer questionId = Integer.valueOf(splittedString.get(0));
                final List<Answer> answersForOneQuestion = new ArrayList<>();
                for (String answerText : splittedString.subList(1, splittedString.size())) {
                    answersForOneQuestion.add(new Answer(answerText));
                }
                answers.put(questionId, answersForOneQuestion);
            }
            streamReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return answers;
    }
}
