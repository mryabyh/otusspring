package ru.otus.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.otus.domain.Question;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Component
public class QuestionDaoImpl implements QuestionDao {
    private final LoaderDao loaderDao;

    @Autowired
    public QuestionDaoImpl(LoaderDao loaderDao) {
        this.loaderDao = loaderDao;
    }

    @Override
    public Collection<Question> loadAllQuestions() {
        Collection<Question> questions = new ArrayList<>();
        InputStreamReader streamReader = loaderDao.loadQuestions();
        BufferedReader reader = new BufferedReader(streamReader);
        try {
            for (String line; (line = reader.readLine()) != null; ) {
                final List<String> splittedString = Arrays.asList(line.split(";"));
                questions.add(new Question(Integer.valueOf(splittedString.get(0)), splittedString.get(1), splittedString.get(2)));
            }
            streamReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return questions;
    }
}
