package ru.otus.dao;

import ru.otus.domain.Question;

import java.util.Collection;

public interface QuestionDao {
    Collection<Question> loadAllQuestions();
}
