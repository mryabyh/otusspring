package ru.otus.dao;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

@Component
public class LoaderDaoImpl implements LoaderDao {

    private final String pathToQuestions;
    private final String pathToAnswers;
    private final Locale locale;
    private final String csvExtention = ".csv";

    public LoaderDaoImpl(@Value("${path-to-questions}") String pathToQuestions,
                         @Value("${path-to-answers}") String pathToAnswers,
                         @Value("${locale.language}") String language,
                         @Value("${locale.country}") String country) {
        this.pathToQuestions = pathToQuestions;
        this.pathToAnswers = pathToAnswers;
        this.locale = new Locale(language, country);
    }

    @Override
    public InputStreamReader loadQuestions() {
        return loadResource(pathToQuestions + "_" + locale + csvExtention);
    }

    @Override
    public InputStreamReader loadAnswers() {
        return loadResource(pathToAnswers + "_" + locale + csvExtention);
    }

    private InputStreamReader loadResource(String resourceName) {
        final ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
        InputStream is = systemClassLoader.getResourceAsStream(resourceName);
        return new InputStreamReader(is, StandardCharsets.UTF_8);
    }
}
