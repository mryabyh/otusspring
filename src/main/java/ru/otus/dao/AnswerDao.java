package ru.otus.dao;

import ru.otus.domain.Answer;

import java.util.Collection;
import java.util.Map;

public interface AnswerDao {
    Map<Integer, Collection<Answer>> loadAllAnswers();
}
