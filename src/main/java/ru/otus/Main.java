package ru.otus;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import ru.otus.configs.ApplicationProperties;
import ru.otus.domain.Question;
import ru.otus.service.QuestionService;
import ru.otus.service.QuizService;

import java.util.Collection;

@SpringBootApplication
@EnableConfigurationProperties(ApplicationProperties.class)
public class Main {

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext context) {
        return args -> {
            QuestionService questionService = context.getBean(QuestionService.class);
            QuizService quizService = context.getBean(QuizService.class);
            Collection<Question> questions = questionService.getAllQuestions();

            while (true) {
                quizService.runQuiz(questions);
            }

        };
    }
}
