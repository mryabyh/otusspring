package ru.otus.domain;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Collection;

@Getter
public class Question {
    private Integer id;
    private String text;
    private String rightAnswerText;
    private ArrayList<Answer> answers;

    public Question(Integer id, String text, String rightAnswerText) {
        this.id = id;
        this.text = text;
        this.rightAnswerText = rightAnswerText;
        answers = new ArrayList<>();
    }

    public void addAnswers(Collection<Answer> answers) {
        this.answers.addAll(answers);
    }
}
