package ru.otus.configs;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "locale")
public class LocaleProperties {

    private String language;
    private String country;
}
