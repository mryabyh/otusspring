package ru.otus.configs;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties
public class ApplicationProperties {

    private String pathToQuestions;
    private String pathToAnswers;
    private String uiFileName;
    private LocaleProperties locale;
}
