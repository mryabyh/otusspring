package ru.otus.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.otus.dao.AnswerDao;
import ru.otus.dao.QuestionDao;
import ru.otus.domain.Answer;
import ru.otus.domain.Question;

import java.util.Collection;
import java.util.Map;

@Service
public class QuestionServiceImpl implements QuestionService {
    private final AnswerDao answerDao;
    private final QuestionDao questionDao;

    @Autowired
    public QuestionServiceImpl(QuestionDao questionDao, AnswerDao answerDao) {
        this.questionDao = questionDao;
        this.answerDao = answerDao;
    }

    @Override
    public Collection<Question> getAllQuestions() {
        final Map<Integer, Collection<Answer>> answers = answerDao.loadAllAnswers();
        final Collection<Question> questions = questionDao.loadAllQuestions();

        return merge(answers, questions);
    }

    private Collection<Question> merge(Map<Integer, Collection<Answer>> answers, Collection<Question> questions) {
        for (Question question : questions) {
            question.addAnswers(answers.get(question.getId()));
        }
        return questions;
    }
}
