package ru.otus.service;

public interface LocaleMessageService {
    String getGreetingsMessage();

    String getEnterFirstNameMessage();

    String getEnterLastNameMessage();

    String getEnterRightNumberMessage();

    String getEnterTryAgainMessage();

    String getStudentNameMessage(String firstName, String lastName);

    String getResultMessage(int rightAnswers, int wrongAnswers);
}
