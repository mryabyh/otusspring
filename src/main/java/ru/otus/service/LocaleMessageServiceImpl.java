package ru.otus.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
public class LocaleMessageServiceImpl implements LocaleMessageService {

    private final Locale locale;
    private MessageSource messageSource;
    private final String uiFileName;

    public LocaleMessageServiceImpl(@Value("${locale.language}") String language,
                                    @Value("${locale.country}") String county,
                                    @Value("${uiFileName}") String uiFileName,
                                    MessageSource messageSource) {
        locale = new Locale(language, county);
        this.uiFileName = uiFileName;
        this.messageSource = messageSource;
    }

    @Override
    public String getGreetingsMessage() {
        return messageSource.getMessage("greetings", new String[]{}, locale);
    }

    @Override
    public String getEnterFirstNameMessage() {
        return messageSource.getMessage("enter.firstName", new String[]{}, locale);
    }

    @Override
    public String getEnterLastNameMessage() {
        return messageSource.getMessage("enter.lastName", new String[]{}, locale);
    }

    @Override
    public String getEnterRightNumberMessage() {
        return messageSource.getMessage("enter.rightNumber", new String[]{}, locale);
    }

    @Override
    public String getEnterTryAgainMessage() {
        return messageSource.getMessage("enter.tryAgain", new String[]{}, locale);
    }

    @Override
    public String getResultMessage(int rightAnswers, int wrongAnswers) {
        return messageSource.getMessage("testResult", new Integer[]{rightAnswers, wrongAnswers}, locale);
    }

    @Override
    public String getStudentNameMessage(String firstName, String lastName) {
        return messageSource.getMessage("studentName", new String[]{firstName, lastName}, locale);
    }
}
