package ru.otus.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.otus.domain.Answer;
import ru.otus.domain.Question;
import ru.otus.domain.User;

import java.util.Collection;

@Service
public class QuizServiceImpl implements QuizService {
    private final UserInterfaceService uiService;
    private final LocaleMessageService localeMessageService;

    @Autowired
    public QuizServiceImpl(UserInterfaceService uiService, LocaleMessageService localeMessageService) {
        this.uiService = uiService;
        this.localeMessageService = localeMessageService;
    }

    @Override
    public void runQuiz(Collection<Question> questions) {
        int rightAnswers = 0;
        int wrongAnswers = 0;
        User student = getUser();

        for (Question question : questions) {
            if(processOneQuestion(question)) {
                rightAnswers++;
            } else {
                wrongAnswers++;
            }
        }

        uiService.printLineToUser(localeMessageService.getStudentNameMessage(student.getFirstName(), student.getLastName()));
        uiService.printLineToUser(localeMessageService.getResultMessage(rightAnswers, wrongAnswers));
        uiService.printLineToUser(localeMessageService.getEnterTryAgainMessage());
        uiService.getLineFromUser();
    }

    private User getUser() {
        uiService.printLineToUser(localeMessageService.getGreetingsMessage());
        uiService.printLineToUser(localeMessageService.getEnterFirstNameMessage());
        String firstName = uiService.getLineFromUser();
        uiService.printLineToUser(localeMessageService.getEnterLastNameMessage());
        String lastName = uiService.getLineFromUser();
        return new User(firstName, lastName);
    }

    private void printAnswers(Question question) {
        int answerIndex = 1;
        for (Answer answer : question.getAnswers()) {
            uiService.printLineToUser(answerIndex + " " + answer.getText());
            answerIndex++;
        }
    }

    @Override
    public boolean processOneQuestion(Question question) {
        uiService.printLineToUser(question.getText());
        printAnswers(question);
        uiService.printLineToUser(localeMessageService.getEnterRightNumberMessage());
        String studentAnswer = uiService.getLineFromUser();
        Integer answerCode;
        try {
            answerCode = Integer.valueOf(studentAnswer);
        } catch (NumberFormatException e) {
            answerCode = -1;
        }
        return answerCode > 0 && answerCode <= question.getAnswers().size() &&
                checkAnswer(question, question.getAnswers().get(answerCode - 1).getText());

    }

    private Boolean checkAnswer(Question question, String answer) {
        return question.getRightAnswerText().equals(answer);
    }
}
