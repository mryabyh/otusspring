package ru.otus.service;

import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class ConsoleUserInterface implements UserInterfaceService {

    @Override
    public String getLineFromUser() {
        return new Scanner(System.in).nextLine();
    }

    @Override
    public void printLineToUser(String line) {
        System.out.println(line);
    }
}
