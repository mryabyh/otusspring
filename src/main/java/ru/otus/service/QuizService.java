package ru.otus.service;

import ru.otus.domain.Question;

import java.util.Collection;

public interface QuizService {
    void runQuiz(Collection<Question> questions);
    boolean processOneQuestion(Question question);
}
