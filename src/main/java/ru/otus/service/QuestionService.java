package ru.otus.service;

import ru.otus.domain.Question;

import java.util.Collection;

public interface QuestionService {
    Collection<Question> getAllQuestions();
}
