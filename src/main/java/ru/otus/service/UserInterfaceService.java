package ru.otus.service;

public interface UserInterfaceService {
    String getLineFromUser();
    void printLineToUser(String line);
}
